from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import Store
from .serializers import StoreSerializer
from rest_framework.status import HTTP_201_CREATED
from rest_framework.views import APIView
import datetime


@api_view(http_method_names=['GET'])
def date_today(request):
    cur = datetime.datetime.now()
    return_dict = {'date': cur.strftime('%d/%m/%Y'),
                   'year': cur.strftime('%Y'),
                   'month': cur.strftime('%m'),
                   'day': cur.strftime('%d')}
    return Response(return_dict)


@api_view(http_method_names=['GET'])
def hello_world(request):
    return_dict = {'msg': 'Hello World'}
    return Response(return_dict)


@api_view(http_method_names=['GET'])
def my_name(request):
    name_of_hacker = "Olexandr Malykh"
    return Response({'name': name_of_hacker})


@api_view(http_method_names=['POST'])
def calculator(request):
    action = request.data['action']
    number1 = request.data['number1']
    number2 = request.data['number2']
    if action in ['minus', 'plus', 'divide', 'multiply']:
        if action == 'plus':
            result = number1 + number2
            return Response({'result': result})
        elif action == 'minus':
            result = number1 - number2
            return Response({'result': result})
        elif action == 'divide':
            if number2 != 0:
                result = number1 / number2
                return Response({'result': result})
            else:
                return Response(['При діленні number2 не може дорівнювати 0 (Ділення на 0 неможливе)'],
                                status=400)
        elif action == 'multiply':
            result = number1 * number2
            return Response({'result': result})
    else:
        return Response(['action може бути тільки minus, plus, divide or multiply'],
                        status=400)


class StoreApiView(APIView):

    def get(self, request, format=None):
        stores = Store.objects.all()
        serializer = StoreSerializer(stores, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = StoreSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        print(serializer.save())
        return Response(status=HTTP_201_CREATED, data=serializer.data)
